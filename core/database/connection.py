from pymysql import connect, cursors

# globalna promenljiva connection
connection = None

def open_connection(host="localhost", user="root", password="root", database="db"):
    global connection
    connection = connect(host=host,
                         user=user,
                         password=password,
                         database=database,
                         charset="utf8mb4",
                         cursorclass=cursors.DictCursor)
    return connection

def execute_statement(sql_statement, args=None):
    """
    Funkcija koja izvrsava upis ka bazi
    :param sql_statement: SQL upit
    :param args: lista argumenata koji ce doci na promenljive u upitu
    :return: None (za sve osim SELECT upita) ili rezultat SELECT upita
    """
    result = None
    with connection:
        with connection.cursor() as cursor:
            if sql_statement.startswith("SELECT"):
                cursor.execute(sql_statement, args)
                # dobavljanje svih redova koji zadovoljavaju uslove
                result = cursor.fetchall()
            elif sql_statement.startswith("INSERT") or sql_statement.startswith("DELETE") \
                or sql_statement.startswith("CREATE") or sql_statement.startswith("UPDATE"):
                cursor.execute(sql_statement, args)
                connection.commit()
    # zatvaranje konekcije
    return result

if __name__ == "__main__":
    # segment koda za potrebe testiranja konekcije
    from statements import *
    # connection = open_connection()
    # students = execute_statement("SELECT * FROM `student`")
    # print(students)
    # select_statement = form_select_statement("student", ["ime", "prezime", "jmbg"], {"ime": "ASC"})
    # connection = open_connection()
    # students = execute_statement(select_statement)
    # print(students)
    # connection = open_connection()
    # tables = execute_statement(get_all_tables())
    # print(tables)
    # connection = open_connection()
    # primary_keys = execute_statement(get_primary_keys('student', 'db'))
    # print(primary_keys)
    # connection = open_connection()
    # columns = execute_statement(get_all_columns("student"))
    # print(columns)
    # connection = open_connection()
    # update_statement = form_updata_statement("student", ["prezime"], ["Stevanovic"], "`ime`='Marko'")
    # execute_statement(update_statement)
    # print(update_statement)
    # connection = open_connection()
    # insert_statement = form_insert_statement("student", ["idstudent","ime", "prezime", "jmbg"], ["", "Sava", "Savanovic", "0404004773632"] , ["idstudent"])
    # print(insert_statement)
    # execute_statement(insert_statement)
    # execute_statement("INSERT INTO `student` (`ime`, `prezime`, `jmbg`) VALUES (%s, %s, %s)", ("Sava", "Savanovic", "0404004773632"))
    # print(students)
    # connection = open_connection()
    # execute_statement(form_delete_statement("student", ["ime", "prezime"], ["Sava", "Savanovic"], "AND"))
    # execute_statement("DELETE FROM `student` WHERE `ime`=%s", ("Sava"))
    # print(students)

    # print(form_insert_statement("student", ["ime", "prezime", "jmbg"], ["Sava", "Savanovic", "0404004773632"]))
    # print(form_delete_statement("student", ["ime", "prezime"], ["Sava", "Savanovic"], "AND"))

    connection = open_connection()
    statement = get_not_null_columns("predmet", "db")
    not_nullable_columns = execute_statement(statement)
    print(not_nullable_columns)

    # connection = open_connection()
    # statement = get_autoincrement_columns("predmet", "db")
    # autoincrement_columns = execute_statement(statement)
    # print(autoincrement_columns)