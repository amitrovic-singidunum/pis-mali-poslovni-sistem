# PIS Mali poslovni sistem

Repozitorijum predmetnog projekta iz Projektovanja informacionih sistema.

Generacija: 2022/23.

## Opis projekta
Projekat se izvodi po predefinisanom planu i programu koji se može pronaći u datoteci: https://singimail.sharepoint.com/:b:/s/msteams_f6ee74/EbsspTTitbJGpbswfK6VMyMBSHFdf_xWDS15yKV-npEyPg?e=SEYm8u

Specifikacija predmetnog projekta se nalazi na linku: https://singimail.sharepoint.com/:b:/s/msteams_f6ee74/EZt4MgZYikVKms6g3SHq_dQBm6db-2YbZZpUsp0cQ-hVHw?e=oT1FZ3

## Priprema aplikacije za pokretanje

1. Preuzeti Python verzije 3.10. sa zvaničnog repozitorijuma (https://www.python.org/downloads/release/python-31010/).
2. Instalirati Python i u instalaciji štiklirati Add Python to PATH.
3. Nakon što je dodat u PATH otvoriti CMD (Command prompt) i uneti naredbu: ```python -m pip install PySide2```
4. Nakon toga instalirati biblioteku PyMSQL: ```python -m pip install PyMySQL```
5. Instalirati MySQL (https://dev.mysql.com/downloads/installer/). Instalacijom obuhvatiti i Workbench
6. Pokrenuti Workbench (ili preko konzole aktivirati pristup bazi podataka)
7. Otvoriti lokalni repozitorijum i pokrenuti ```main.py``` datoteku.

## Podešavanje radnog orkuženja
Za potrebe predmetnog projekta će se koristiti okruženje Visual Studio Code (https://code.visualstudio.com/).
Nakon instalacije dodati i proširenje za Python (https://marketplace.visualstudio.com/items?itemName=ms-python.python)
Pokretanje se vrši klikom na Play dugme (gornji levi ugao) ili Run Python File in Terminal.

## Pokretanje aplikacije
Aplikaciju pokrećemo izvršavanjem ```main.py``` u okviru Visual Studio Code aplikacije.

## Autori
Predmetni nastavnici:
1. prof. dr Branko Perišić
2. MSc Aleksandra Mitrović

Studenti:
1. Aleksandra Popović
2. Marina Jasika

## Korisni linkovi
- Dokumentacija PySide2 biblioteke: https://doc.qt.io/qtforpython-5/contents.html
- Dokumentacija PyMySQL biblioteke: https://pymysql.readthedocs.io/en/latest/user/index.html
- Dokumentacija MySQL Workbench https://www.mysql.com/products/workbench/ (u dnu imaju video tutorijali)

## Licenca
Projekat otvorenog koda.

## Status projekta
Razvoj na projektu je aktivan.
