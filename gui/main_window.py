from PySide2.QtWidgets import QMainWindow, QLabel
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt
from gui.widgets.central_widget import CentralWidget
from gui.widgets.menu_bar import MenuBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.tool_bar import ToolBar
from gui.widgets.structure_dock_widget import StructureDockWidget


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        # poziv super inicijalizaotra (QMainWindow)
        super().__init__(parent)
        # Osnovna podesavanja glavnog prozora
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("resources/icons/blue-document.png"))
        self.resize(1000, 800)

        # inicijalizacija osnovnih elemenata GUI-ja
        self.tool_bar = ToolBar(parent=self)
        self.menu_bar = MenuBar(self)
        self.status_bar = StatusBar(self)
        self.central_widget = CentralWidget(self)
        self.file_dock_widget = StructureDockWidget("Struktura radnog prostora", self)

        # uvezivanje elemenata GUI-ja
        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.file_dock_widget, Qt.Vertical)

        # sacuvavanje prijavljenog korisnika iz dijaloga
        self.status_bar.addWidget(QLabel("Ulogovani korisnik: " + "Aleksandra Mitrovic"))

    def get_actions(self):
        return self.tool_bar.actions_dict
    
    def add_actions(self, actions, where="toolbar", menu="File"):
        self.tool_bar.addSeparator()
        if where == "toolbar":
            self.tool_bar.addActions(actions)
        elif where == "menubar":
            # TODO: dodati akciju u odgovarjauci menu bar
            for action in actions:
                self.menu_bar.add_action(action, menu)
