from PySide2.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QDialogButtonBox, QLabel, QLineEdit, QPushButton, QFileDialog, QListView, QMessageBox
from PySide2.QtGui import QIcon
from gui.widgets.create_file_dialog.columns_list_model import ColumnsListModel

class CreateFileDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Create new file")
        self.setWindowIcon(QIcon("resources/icons/document--plus.png"))
        self.resize(600, 200)

        self.dialog_layout = QVBoxLayout()
        self.file_layout = QHBoxLayout()
        self.file_name_layout = QHBoxLayout()

        self.folder_path = QLineEdit(self)
        self.folder_path.setReadOnly(True)
        self.browse_button = QPushButton("Browse...", self)
        self.browse_button.clicked.connect(self.on_browse_button)

        self.file_name = QLineEdit(self)

        self.column_layout = QHBoxLayout()
        self.column_name_edit = QLineEdit(self)
        self.add_column_button = QPushButton(QIcon("resources/icons/plus.png"), "Add column", self)
        self.add_column_button.clicked.connect(self.on_add_column_button)

        self.columns = QListView(self)
        self.columns_model = ColumnsListModel([])
        self.columns.setModel(self.columns_model)

        self.file_layout.addWidget(QLabel("File path:"))
        self.file_layout.addWidget(self.folder_path)
        self.file_layout.addWidget(self.browse_button)

        self.column_layout.addWidget(QLabel("Column name:"))
        self.column_layout.addWidget(self.column_name_edit)
        self.column_layout.addWidget(self.add_column_button)

        self.file_name_layout.addWidget(QLabel("File name:"))
        self.file_name_layout.addWidget(self.file_name)


        # TODO: populisanje layout-a
        # dugmici
        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        # uvezivanje funkcija koje ce se pozivati kada se okine odredjeni dugmic
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.dialog_layout.addLayout(self.file_layout)
        self.dialog_layout.addLayout(self.file_name_layout)
        self.dialog_layout.addSpacing(20)
        self.dialog_layout.addLayout(self.column_layout)
        self.dialog_layout.addWidget(self.columns)
        self.dialog_layout.addWidget(self.button_box)
        self.setLayout(self.dialog_layout)

    def on_browse_button(self):
        folder_path = QFileDialog.getExistingDirectory(self, "Save to...")
        self.folder_path.setText(folder_path)

    def on_add_column_button(self):
        # FIXME: proveriti da li takav naziv kolone vec postoji u modelu (obezbediti jedinstvenost)
        # URADJENO
        if self.column_name_edit.text() not in self.columns_model.columns:
            self.columns_model.insert_row(self.columns_model.rowCount(), self.column_name_edit.text())
        else:
            QMessageBox.warning(self, "Duplicate column name", "Columns should be unique!", QMessageBox.Ok)

    def get_data(self):
        data = {
            "folder_path": self.folder_path.text(),
            "file_name": self.file_name.text() + ".csv",
            "columns": self.columns_model.columns
        }
        return data
    
    def reset_model(self):
        self.columns_model.delete_rows()
        self.columns.update()
    
    def accept(self):
        data = self.get_data()
        if data["folder_path"] == "":
            QMessageBox.warning(self, "Data missing", "Folder path should be provided!", QMessageBox.Ok)
            return
        elif data["file_name"] == ".csv": # nema naziva
            QMessageBox.warning(self, "Data missing", "File name should be provided!", QMessageBox.Ok)
            return
        elif len(data["columns"]) == 0:
            QMessageBox.warning(self, "Data missing", "File should have columns in a header!", QMessageBox.Ok)
            return
        else:
            return super().accept()
    