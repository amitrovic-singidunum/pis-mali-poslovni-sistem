from PySide2.QtCore import QAbstractListModel, QModelIndex, Qt


class ColumnsListModel(QAbstractListModel):
    def __init__(self, columns=[], parent=None):
        super().__init__(parent)
        self.columns = columns # lista stringova

    def get_element(self, index:QModelIndex):
        if index.isValid():
            element = self.columns[index.row()]
            if element:
                return element
        return self.columns
    
    def insert_row(self, position=0, data=""):
        self.beginInsertRows(QModelIndex(), position+1, position+1)
        self.columns.insert(position+1, data)
        self.endInsertRows()
    
    def delete_rows(self):
        self.beginRemoveRows(QModelIndex(), 0, self.rowCount())
        self.columns = []
        self.endRemoveRows()

    def rowCount(self, parent= ...) -> int:
        if self.columns is None:
            return 0
        return len(self.columns)
    
    def data(self, index: QModelIndex, role: int = ...):
        element = self.get_element(index)
        if role == Qt.DisplayRole:
            return element
        
    def headerData(self, section: int, orientation:Qt.Orientation, role: int = ...):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return "Column name"