from PySide2.QtWidgets import QWidget, QVBoxLayout, QTextEdit
# from gui.widgets.table_widget.test_table_widget import TestTableWidget
from gui.widgets.data_view.data_view import DataView


class CentralWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.cw_layout = QVBoxLayout(self)
        self.table = DataView()

        # dodavanje akcija u toolbar (parent je MainWindow)
        self.parent().add_actions(self.table.export_actions())
        self.parent().add_actions(self.table.export_actions(), "menubar", "File")

        self.cw_layout.addWidget(self.table)

        # obavezno dati layout uvezujemo na widget (CentralWidget)
        self.setLayout(self.cw_layout)