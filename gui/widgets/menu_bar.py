from PySide2.QtWidgets import QMenuBar, QMenu

class MenuBar(QMenuBar):
    def __init__(self, parent=None):
        # poziv super inicijalizatora
        super().__init__(parent)

        # kreiranje osnovnih menija
        self.file_menu = QMenu("File", self)
        self.edit_menu = QMenu("Edit", self)
        self.help_menu = QMenu("Help", self)

        # povezivanje menija
        self._poulate_menues()

    def _poulate_menues(self):
        # dodavanje akcija u meni
        actions = self.parent().get_actions()

        self.help_menu.addAction(actions["help"])
        self.help_menu.addAction(actions["about"])

        self.addMenu(self.file_menu)
        self.addMenu(self.edit_menu)
        self.addMenu(self.help_menu)

    def add_action(self, action, menu="File"):
        if menu == "File":
            self.file_menu.addAction(action)
        elif menu == "Edit":
            self.edit_menu.addAction(action)
        elif menu == "Help":
            self.help_menu.addAction(action)