from PySide2.QtCore import QAbstractTableModel, QAbstractItemModel, QModelIndex, Qt

class DataModel(QAbstractTableModel):
    def __init__(self, parent=None, data=None, header_data=None):
        super().__init__(parent)
        self.table_data = data
        self.header_data = header_data

    def from_dict(self, data):
        """
        Metoda koja na osnovu liste recnika pravi model podataka
        :param data: lista recnika
        """
        if len(data) > 0:
            # smestamo kljuceve sa prvog recnika
            self.header_data = list(data[0].keys())
            self.table_data = []
            for row in data:
                # row je recnik od kojeg uzimamo vrednosti da bismo kreirali red u tabeli
                self.table_data.append(list(row.values()))
    
    def get_row(self, row=0):
        # metoda sluzi za dobavljanje podataka iz reda
        # ako je prosledjeni red u okviru granica tabele (njenog broja redova)
        if self.table_data is not None:
            if row < len(self.table_data):
                data_row = self.table_data[row] # lista elemenata
                if data_row:
                    return data_row
                
    def get_all_rows(self):
        return self.table_data
                
    def get_headers(self):
        return self.header_data
    
    def replace_data(self, row, data=[]):
        # emitovati signal za promenu
        # izvrsiti promenu
        self.table_data[row] = data
        # emitovati signal za kraj promene

    def delete_row(self, row):
        # uklanjanje reda iz liste (matrice)
        # naglasavanje da se desava promena uklanja redova
        self.beginRemoveRows(QModelIndex(), row, row)
        self.table_data.pop(row)
        self.endRemoveRows()

    def insert_row(self, row, data):
        # umetanje novog reda nakon pozicije row sa podacima data
        self.beginInsertRows(QModelIndex(), row+1, row+1)
        self.table_data.insert(row+1, data)
        self.endInsertRows()
        
    # sopstvena metoda (ne redefinise se)
    def get_element(self, index: QModelIndex):
        """
        :param index: indeks sa kojeg se dobavlja element (sadrzaj)
        :return: sadrzaj za datu celiju / cela lista (u slucaju da je indeks nevalidan)
        """
        if index.isValid():
            # element iz matrice dobijamo spram reda i kolone indeksa
            element = self.table_data[index.row()][index.column()]
            if element:
                return element
        return self.table_data

    # redefinisanje neophodnih metoda za pravljenje naseg modela
    def rowCount(self, parent=...):
        if self.table_data is None:
            return 0
        return len(self.table_data) # koliko ima redova (podlista) u listi
    
    def columnCount(self, parent=...):
        if self.header_data is None:
            return 0
        return len(self.header_data)
    
    def data(self, index, role=...):
        element = self.get_element(index)
        if role == Qt.DisplayRole: # Qt.DecorationRole (za ikonice)
            return element # sadrzaj celije
        
    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header_data[section] # section == kolona
        elif orientation == Qt.Vertical:
            return super().headerData(section, orientation, role)
        
    # pravljenje editable modela
    # koristi se kada zelimo direktno u tabeli da obezbedimo promene
    # def setData(self, index: QModelIndex, value, role: int = ...) -> bool:
    #     return super().setData(index, value, role)
    #     # super().dataChanged()
    
    # def flags(self, index: QModelIndex) -> Qt.ItemFlags:
    #     return super().flags(index) | Qt.ItemIsEditable
