from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableView, QAction, QFileDialog, QAbstractItemView, QMessageBox
from PySide2.QtGui import QIcon
from gui.widgets.data_view.data_model import DataModel
from gui.widgets.edit_dialog.edit_dialog import EditDialog
from gui.widgets.database_dialog.database_dialog import DatabaseDialog
from gui.widgets.database_dialog.table_selection_dialog import TableSelectionDialog
from gui.widgets.create_file_dialog.create_file_dialog import CreateFileDialog
from core.database.connection import open_connection, connection, execute_statement
from core.database.statements import form_select_statement, form_update_statement, form_insert_statement, get_all_tables, get_primary_keys, get_not_null_columns
import csv
import os


class DataView(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.widget_layout = QVBoxLayout()
        self.table_view = QTableView()

        # definisanje i uvezivanje akcija koje se mogu uraditi nad prikazom podataka
        self.open_file_action = QAction(QIcon("resources/icons/folder-open-document.png"), "Open CSV file")
        self.open_file_action.triggered.connect(self.on_open_file_action)
        self.create_file_action = QAction(QIcon("resources/icons/document--plus.png"), "Create new file")
        self.create_file_action.triggered.connect(self.on_create_file_action)
        self.open_database_action = QAction(QIcon("resources/icons/database.png"), "Open database")
        self.open_database_action.triggered.connect(self.on_open_database_action)
        self.open_table_action = QAction(QIcon("resources/icons/database-property.png"), "Open table")
        self.open_table_action.triggered.connect(self.on_open_table_action)
        self.save_file_action = QAction(QIcon("resources/icons/disk.png"), "Save CSV file")
        self.save_file_action.triggered.connect(self.on_save_file_action)
        self.delete_row_action = QAction(QIcon("resources/icons/table-delete-row.png"), "Delete row")
        self.delete_row_action.triggered.connect(self.on_delete_row_action)
        self.insert_row_action = QAction(QIcon("resources/icons/table-insert-row.png"), "Insert row")
        self.insert_row_action.triggered.connect(self.on_insert_row_action)
        self.table_view.doubleClicked.connect(self.open_edit_form)

        # podesavanje tabelarnog prikaza tako da se vrsi selekcija na nivou jednog celog reda
        self.table_view.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table_view.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.widget_layout.addWidget(self.table_view)
        self.setLayout(self.widget_layout)

        # podaci o konekciji
        self.connection_data = None
        self.data_source_type = None
    
    def on_open_file_action(self):
        # izbor datoteke iz fajl sistema
        file_name = QFileDialog.getOpenFileName(self, "Open data file", 
                                                "/resources/data", 
                                                "CSV files (*.csv)")
        # kada se otvori diajlog a ne izabere datoteka
        if file_name[0] == "":
            return
        # ucitavanje podataka iz datoteke
        with open(file_name[0], "r", encoding="utf-8") as fp:
            reader = csv.reader(fp, delimiter=",")
            reader = list(reader)
            header = reader[0]   
            data = reader[1:]
            # kreiranje modela na osnovu ucitanih podataka
            self.table_model = DataModel(data=data, header_data=header)
            # postavljanje modela u prikaz
            self.table_view.setModel(self.table_model)
            self.data_source_type = "file"

    def on_create_file_action(self):
        dialog = CreateFileDialog(self.parent())
        result = dialog.exec_()
        if result == 1:
            # TODO: kreirati datoteku sa nazivima kolona koji su uneti u listi
            data = dialog.get_data()
            with open(os.path.join(data["folder_path"], data["file_name"]), "w", encoding="utf-8") as fp:
                writer = csv.writer(fp, delimiter=",")
                # zapisivanje zaglavlja datoteke
                writer.writerow(data["columns"])
            dialog.reset_model()
    
    def on_save_file_action(self):
        file_name = QFileDialog.getSaveFileName(self, "Save CSV file",
                                       "/resources/data/untitled.csv",
                                       "CSV file (*.csv)")
        # kada se otvori diajlog a ne izabere datoteka
        if file_name[0] == "":
            return
        with open(file_name[0], "w", encoding="utf-8", newline="") as fp:
            writer = csv.writer(fp, delimiter=",")
            # dobavljanje modela iz kog ce se iscitati header i linije podatakas
            model = self.table_view.model()
            writer.writerow(model.get_headers())
            # zapisivanje svih podataka
            writer.writerows(model.get_all_rows())

    def on_open_database_action(self):
        database_dialog = DatabaseDialog(self)
        result = database_dialog.exec_()
        if result == 1:
            self.connection_data = database_dialog.get_data()
            connection = open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
            if connection is not None:
                QMessageBox.information(self, "Connection to a database", "Connection to database " + self.connection_data["db_name"] + " is successful!")

    def on_open_table_action(self):
        if self.connection_data is None:
            QMessageBox.warning(self, "Connection failed", "First connect to a database to search for tables!")
            return
        table_selection_dialog = TableSelectionDialog(self)
        open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
        sql_tables = execute_statement(get_all_tables(schema=self.connection_data["db_name"]))
        tables = []
        for table in sql_tables:
            tables.append(table["TABLE_NAME"])
        table_selection_dialog.populate_tables(tables)
        result = table_selection_dialog.exec_()
        if result == 1:
            table = table_selection_dialog.get_data()
            open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
            select_statement = form_select_statement(table)
            # FIXME: ako tabela u bazi nema redova, javlja se greska
            data = execute_statement(select_statement)
            # TODO: popuniti model za prikaz u glavnom widget-u
            # kreiranje modela na osnovu ucitanih podataka
            # FIXME: dobaviti koje se kolone prikazuju za header
            self.table_model = DataModel()
            self.table_model.from_dict(data)
            # postavljanje modela u prikaz
            self.table_view.setModel(self.table_model)
            self.data_source_type = "database"
            self.connection_data["table"] = table


    def on_delete_row_action(self):
        # dobaviti selektovani red
        # ukoliko nema selekcije obavestiti korisnika da je brisanje moguce samo za odbrani red
        # na osnovu broja reda iz modela ukloniti taj element (listu na poziciji u matrici)
        selected_indexes = self.table_view.selectedIndexes()
        if selected_indexes is None or len(selected_indexes) == 0:
            # obavestiti korisnika da se brisanje vrsi samo za odabrani red
            QMessageBox.warning(self.parent(), "Warning", "Row must be selected in order to delete a row.", QMessageBox.Ok)
            return
        row = selected_indexes[0].row()
        self.table_model.delete_row(row)

    def on_insert_row_action(self):
        model = self.table_view.model()
        if model is None:
            QMessageBox.warning(self.parent(), "Warning", "Table is missing, open a table to insert a row.", QMessageBox.Ok)
            return
        selected_indexes = self.table_view.selectedIndexes()
        selected_labels = model.get_headers()
        edit_dialog = EditDialog(self.parent())
        # preimenovanje da sluzi za unos podataka
        edit_dialog.setWindowTitle("Insert row")
        # input polja nece imati sadrzaj
        # ako je u pitanju tabela iz baze podataka, proslediti INSERT INTO upit
        if self.data_source_type == "database":
            # FIXME: postaviti da su read-only autoincrement kljucevi
            primary_keys = []
            open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
            data = execute_statement(get_primary_keys(self.connection_data["table"], self.connection_data["db_name"]))
            for pk in data:
                primary_keys.append(pk["column_name"])
            
            edit_dialog.enter_data(selected_labels, [], read_only=primary_keys)
        else:
            edit_dialog.enter_data(selected_labels, [])
        result = edit_dialog.exec_()
        if result == 1:
            dialog_data = edit_dialog.get_data()
            if self.data_source_type == "database":
                # izvrsiti insert into
                open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
                execute_statement(form_insert_statement(self.connection_data["table"], selected_labels, dialog_data, primary_keys))
                # Izvrsavanje SELECT upita zarad dobavljanja reda koji je dodat, a kome je
                # uradjen auto-increment primarnog kljuca
                open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
                select_statement = form_select_statement(self.connection_data["table"])
                data = execute_statement(select_statement)
                self.table_model = DataModel()
                self.table_model.from_dict(data)
                self.table_view.setModel(self.table_model)
            elif self.data_source_type == "file":
                # kada je tabela prazna/nema selekcije, umetanje se vrsi na 0-tu poziciju
                if len(selected_indexes) == 0:
                    row = 0
                # kada nije prazna, umetanje se vrsi u red nakon selektovanog (poslednjeg dodatog)
                else:
                    row = selected_indexes[0].row()
                self.table_model.insert_row(row, dialog_data)


    def export_actions(self):
        return [self.create_file_action, self.open_file_action, self.open_database_action, self.open_table_action, self.save_file_action, self.delete_row_action, self.insert_row_action]
    
    def open_edit_form(self, index=None):
        model = self.table_view.model()
        selected_labels = model.get_headers()
        selected_data = model.get_row(index.row())
        # kreiranje dijaloga
        edit_dialog = EditDialog(self.parent())
        # populisanje dialoga podacima iz reda tabele
        if self.data_source_type == "file":
            edit_dialog.enter_data(selected_labels, selected_data)
        elif self.data_source_type == "database":
            primary_keys = []
            open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
            data = execute_statement(get_primary_keys(self.connection_data["table"], self.connection_data["db_name"]))
            for pk in data:
                primary_keys.append(pk["column_name"])
            not_nullable = []
            open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
            data = execute_statement(get_not_null_columns(self.connection_data["table"], self.connection_data["db_name"]))
            for nn in data:
                not_nullable.append(nn["COLUMN_NAME"])
            edit_dialog.enter_data(selected_labels, selected_data, read_only=primary_keys, not_null=not_nullable)
        # prikazivanje dijaloga
        result = edit_dialog.exec_()
        if result == 1: # izmena prihvacena
            data = edit_dialog.get_data()
            model.replace_data(index.row(), data)
            # TODO: dodati da se prosledi SQL upit ukoliko je tabela iz baze
            if self.data_source_type == "database":
                open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
                execute_statement(form_update_statement(self.connection_data["table"], selected_labels, data, primary_keys))

